<?php


namespace Plusforta\StorageBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;

class PlusfortaStorageExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.xml');

        $configuration = $this->getConfiguration($configs, $container);
        $config = $this->processConfiguration($configuration, $configs);

        $this->setupAwsClient($config, $container);
    }

    private function setupAwsClient(array $config, ContainerBuilder $container): void
    {
        $awsConfig = $config['aws'];
        $awsClientDefinition = $container->getDefinition('plusforta.aws.client');
        $defaults = $awsClientDefinition->getArgument(0);

        $argument = array_merge($defaults, $awsConfig);
        $awsClientDefinition->setArgument(0, $argument);
    }
}
