<?php

namespace Plusforta\StorageBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('plusforta_storage');
        $rootNode = $treeBuilder->getRootNode();
        $rootNode->children()
            ->scalarNode('temporary')->defaultValue('%kernel.project_dir%/var/storage/temporary')->end()
            ->arrayNode('aws')->addDefaultsIfNotSet()->children()
                ->scalarNode('version')->defaultValue('2006-03-01')->end()
                ->scalarNode('region')->defaultValue('eu-central-1')->end()
                ->arrayNode('credentials')->children()
                    ->scalarNode('key')->end()
                    ->scalarNode('secret')->end()
                ->end()
            ->end()
        ->end();
        return $treeBuilder;
    }
}
