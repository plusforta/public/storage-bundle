<?php


namespace Plusforta\StorageBundle\ValueObject\Storage;

use Webmozart\Assert\Assert;

class StorageProvider
{
    public const PDF_STORAGE_AWS = 'pdf.storage.aws';
    public const PDF_STORAGE_LOCAL = 'pdf.storage.local';

    public const EXPORT_STORAGE_AWS = 'export.storage.aws';
    public const EXPORT_STORAGE_GOOGLE = 'export.storage.google';
    public const EXPORT_STORAGE_LOCAL = 'export.storage.local';

    public const TRANSFER_STORAGE_AWS = 'transfer.storage.aws';
    public const TRANSFER_STORAGE_LOCAL = 'transfer.storage.local';

    public const TEMPORARY_STORAGE_LOCAL = 'temporary.storage.local';


    public const ALLOWED_VALUES = [
        self::PDF_STORAGE_AWS,
        self::PDF_STORAGE_LOCAL,
        self::EXPORT_STORAGE_AWS,
        self::EXPORT_STORAGE_GOOGLE,
        self::EXPORT_STORAGE_LOCAL,
        self::TRANSFER_STORAGE_AWS,
        self::TRANSFER_STORAGE_LOCAL,
        self::TEMPORARY_STORAGE_LOCAL,
    ];

    private string $value;

    private function __construct(string $value)
    {
        $this->value = $value;
    }

    public static function fromString(string $value): self
    {
        Assert::oneOf($value, self::ALLOWED_VALUES);

        return new self($value);
    }

    public function toString(): string
    {
        return $this->value;
    }
}
