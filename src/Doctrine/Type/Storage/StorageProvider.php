<?php


namespace Plusforta\StorageBundle\Doctrine\Type\Storage;

use Plusforta\StorageBundle\Doctrine\Type\StringValue;

class StorageProvider extends StringValue
{
    protected const TYPE_NAME = 'storage_provider';

    protected function getClassName(): string
    {
        return \Plusforta\StorageBundle\ValueObject\Storage\StorageProvider::class;
    }
}
