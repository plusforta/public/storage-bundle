<?php


namespace Plusforta\StorageBundle;

use Plusforta\StorageBundle\DependencyInjection\PlusfortaStorageExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class PlusfortaStorageBundle extends Bundle
{
    /**
     * Overridden to allow for the custom extension alias.
     */
    public function getContainerExtension(): ?ExtensionInterface
    {
        if (null === $this->extension) {
            $this->extension = new PlusfortaStorageExtension();
        }

        return $this->extension;
    }
}
