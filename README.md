# Storage Bundle

The storage bundle defines a service locator that uses the storage provider to determine the
Flysystem storage based on the storage provider.

## Installation

```shell
composer require plusforta/storage-bundle
```
## Versions

| Version | Flysystem Version | PHP Version |
|---------|:-----------------:|-------------|
| 1.x     |        1.x        | 7.4         |
| 2.x     |        2.x        | 7.4         |
| 3.x     |        3.x        | 7.4         |

## Usage

### flysystem.yaml Definition

The flysystem.yaml must be defined with the following storages:

```yaml
flysystem:
    storages:
        pdf.storage.aws:
            adapter: 'aws'
            options:
                client: 'plusforta.aws.client' # The service ID of the Aws\S3\S3Client instance
                bucket: '%env(AWS_S3_BUCKET_NAME_PDF)%'
                prefix: '%env(AWS_S3_BUCKET_PREFIX_PATH_PDF)%'
        pdf.storage.local:
            adapter: 'local'
            options:
                directory: '%kernel.project_dir%/var/storage/pdf'
        pdf.storage:
            adapter: 'lazy'
            options:
                source: '%env(PDF_STORAGE_PROVIDER)%' # set this to transfer.storage.sftp to transfer to hr data
        transfer.storage.sftp:
            adapter: 'sftp'
            options:
                host: '%env(AMS_SFTP_TRANSFER_HOST)%'
                port: '%env(int:AMS_SFTP_TRANSFER_PORT)%'
                username: '%env(AMS_SFTP_TRANSFER_USERNAME)%'
                password: '%env(AMS_SFTP_TRANSFER_PASSWORD)%'
                privateKey: '%env(AMS_SFTP_TRANSFER_PRIVATE_KEY)%'
                root: '%env(AMS_SFTP_TRANSFER_ROOT_PATH)%'
                timeout: 10
                directoryPerm: 0744
                permPublic: 0700
                permPrivate: 0744
        transfer.storage.local:
            adapter: 'local'
            options:
                directory: '%kernel.project_dir%/var/storage/transferred'
        transfer.storage:
            adapter: 'lazy'
            options:
                source: '%env(TRANSFER_STORAGE)%' # set this to transfer.storage.sftp to transfer to hr data
        export.storage.aws:
            adapter: 'aws'
            options:
                client: 'plusforta.aws.client' # The service ID of the Aws\S3\S3Client instance
                bucket: '%env(AWS_S3_BUCKET_NAME_EXPORT)%'
                prefix: '%env(AWS_S3_BUCKET_PREFIX_PATH_EXPORT)%'
        export.storage.local:
            adapter: 'local'
            options:
                directory: '%env(TEMP_EXPORT_DIRECTORY)%'
        export.storage:
            adapter: 'lazy'
            options:
                source: '%env(EXPORT_STORAGE)%' # set this to transfer.storage.sftp to transfer to hr data

        export.storage.google:
            adapter: 'local'
            options:
                directory: '%env(TEMP_EXPORT_DIRECTORY)%'
```

### Using the service locator

The ServiceLocator can be used in the project via Depenency Injection:

```php
    public function __construct(ServiceLocator $storageLocator)
```

The ServiceLocator can then use `get` to determine the storage per storage provider.

```php
    $pdfStorage = $this->storageLocator->get('export.storage.pdf');
```
